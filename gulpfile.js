const {src, dest, series, parallel, watch} = require('gulp')
const htmlmin = require('gulp-htmlmin')
const validator = require('gulp-html')
const cleanCss = require('gulp-clean-css')
const concatCss = require('gulp-concat-css')
// const autoprefix = require('gulp-autoprefixer')
const browserSync = require('browser-sync')


const browser = browserSync.create()

// first html task
function html(){
    return src('src/index.html')
        .pipe(validator())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(dest('build/'));
}

function css(){
    return src('src/css/*.css')
        // .pipe(autoprefix({cascade: false}))
        .pipe(concatCss('bundle.css'))
        .pipe(cleanCss())
        .pipe(dest('build/'))
}


function watchFiles(cb){
    // watch('src/**/*.*').on('change', parallel(html, css))
    watch('src/**/*.*', parallel(html, css))
}


function reload(cb){
    browser.reload()
    cb()
}

function live(){
    browser.init({
        server: {
            baseDir: 'build/'
        }
    })

    watch('src/**/*.*', series(html, css, reload))
}


exports.html = html;
exports.css = css;

exports.build = parallel(html, css);
exports.watch = watchFiles
exports.live = live